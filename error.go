/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package errcat

import (
	"errors"
	"strings"
)

const errLabel string = "Error: "
const warnLabel string = "Warning: "
const infoLabel string = "Info: "

// AppendErr appends an error (err) to another error (e) and returns a new error (e + err)
func AppendErr(e, err error) error {
	if e == nil {
		return err
	}
	if err == nil {
		return e
	}
	return errors.New(e.Error() + err.Error())

}

// AppendErrStr appends the errLabel and a string (str) to an error (e) and returns a new error
func AppendErrStr(e error, str string) error {
	return errorAppendStr(e, str, errLabel)
}

// AppendWarnStr appends the warnLabel and a string (str) to an error (e) and returns a new error
func AppendWarnStr(e error, str string) error {
	return errorAppendStr(e, str, warnLabel)
}

// AppendInfoStr appends the infoLabel and a string (str) to an error (e) and returns a new error
func AppendInfoStr(e error, str string) error {
	return errorAppendStr(e, str, infoLabel)
}

// errorAppendStr appends a string (label) and a string (str) to an error (e) and returns a new error
func errorAppendStr(e error, str, label string) error {
	if e == nil {
		return errors.New(label + str + "\n")
	}
	return errors.New(e.Error() + label + str + "\n")

}

// IsErr checks to see if the error (e) contains the errLabel and returns the boolean response
func IsErr(e error) bool {
	return isLabel(e, errLabel)
}

// IsWarn checks to see if the error (e) contains the warnLabel and returns the boolean response
func IsWarn(e error) bool {
	return isLabel(e, warnLabel)
}

// IsInfo checks to see if the error (e) contains the infoLabel and returns the boolean response
func IsInfo(e error) bool {
	return isLabel(e, infoLabel)
}

// isLabel checks to see if the error (e) contains the string (label) and returns the boolean response
func isLabel(e error, label string) bool {
	if e == nil {
		return false
	}
	return strings.Contains(e.Error(), label)

}
